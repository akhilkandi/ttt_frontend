import React, { Component } from 'react';

import './App.css';
import Npage from './Npage';

const BaseLayout = () => (
  <div className="container-fluid">


    <div className="content">
      <Npage />
    </div>

  </div>
)

class App extends Component {
  render() {
    return (
    <BaseLayout/>
    );
  }
}

export default App;
