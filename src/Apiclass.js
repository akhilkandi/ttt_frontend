import axios from 'axios';

const API_URL = 'http://localhost:8000';

export default class Apiclass {
  constructor(){}

  getResult(id){
    const url = `${API_URL}/${id}`;
    return axios.get(url).then(response => response.data);


  }

}
